# codingchallenge-ios
Base URL: 
http://thedemoapp.herokuapp.com

Get all post:
GET /post

Get like count with given post id:
Get /post/:post_id/likeCount

Get comment count with given post id:
Get /post/:post_id/commentCount